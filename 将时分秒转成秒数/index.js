// 时间改变  传入字符串格式  
/* 
 eg:1.10.36  一分十秒 =>  40[number格式]
*/
function timeChange(time) {
    let h = 0;
    let m = 0;
    let s = 0;
    let seconds = 0;

    let length = 0;
    if (time.indexOf(':') !== -1) {
        length = time.split(':').length - 1;
        if (length === 1) {
            m = Number(time.split(':')[0]);
            s = Number(time.split(':')[1]);
            seconds = m * 60 + s;
        } else if (length === 2) {
            h = Number(time.split(':')[0]);
            m = Number(time.split(':')[1]);
            s = Number(time.split(':')[2]);
            seconds = h * 3600 + m * 60 + s;
        }
    }
    return seconds;
}