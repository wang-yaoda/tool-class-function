  // 防抖函数
  function debounce(func, delay) {
    let timer = null;
    // 返回一个新的函数  ...代表可以传入多个变量  闭包  timer是外面引入
    return function (...args) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, delay);
    };
}      
  


// 节流函数
function throttle(callBack, time) {
    // callBack: 传入的回调函数
    // time: 定时器的第二个参数（多长时间触发）
    // 节流阀的状态（默认开启）
    let valve = true
    return function () {
// 如果节流阀处于关闭状态，则不执行里面代码
        if (valve) {
            // 关闭节流阀（在里面代码没执行完之前，不能进入）
            valve = false
            // 开启定时器
            setTimeout(() => {
    // 通过 apply 改变传入的回调函数 callBack 的 this 指向
// 通过 arguments 拿到外层函数的事件对象（箭头函数没有 this 指向）
// 注意：事件处理函数只能有一个形参（拿到事件对象），
// 如果要传多个参数，方法:在外面封装一个函数，在事件处理函数中调用
callBack.apply(this, arguments)
// 当回调函数执行完，开启节流阀（可以执行进入里面执行传入的回调函数了）
   valve = true
              }, time)
        }
    }
}