## vue源码学习

```js
事实上Vue的源码包含三大核心
Compiler模块：编译模板系统；
Runtime模块：也可以称之为Renderer模块，真正渲染的模块；
Reactivity模块：响应式系统；
/*
个人感觉 babel大概的原理就类似于这个compiler一样 先是词法分析 语法分析 => 旧AST语法树 =>新AST语法树
不考虑compiler的实现 因为他转换成AST语法树 进行编译 和正则校验 
compiler 开发过程中 .vue文件 直接交给@vue/compiler-sfc插件来实现了
*/
```

### 渲染系统的实现

```js
渲染系统，该模块主要包含三个功能： //runtime => vnode =>真实dom
功能一：h函数，用于返回一个VNode对象；
功能二：mount函数，用于将VNode挂载到DOM上；
功能三：patch函数，用于对两个VNode进行对比，决定如何处理新的VNode；
```

#### h函数思路

（h函数，用于返回一个VNode对象；）

```js
h() 函数是一个用于创建 VNode 的实用程序。也许可以更准确地将其命名为 createVNode()，但由于频繁使用和简洁，它被称为 h()
文档地址：https://v3.cn.vuejs.org/guide/render-function.html#h-%E5%8F%82%E6%95%B0
1.上述是官方文档的介绍
2.h函数传入三个参数 1.标签名，2.props(一些class id 或者自身的一些dom事件) 3.传入{String | Array | Object} children
3.h函数返回一个vnode节点 而vnode节点就是一个JavaScript对象，所以由以上的思路 我们来实现
```

#### h函数代码实现

```js
const h = (tag,props,children)=>{
     //vnode就是一个JavaScript对象 h函数返回一个vnode
    return {
        tag,
        props,
        children
    }
}
```

#### mount函数思路

（mount函数，用于将VNode挂载到DOM上）

```js
mount函数的实现：
第一步：根据tag，创建HTML元素，并且存储到vnode的el中；

第二步：处理props属性 判断props里面的属性 
如果以on开头，那么监听事件； 普通属性直接通过 setAttribute 添加即可；

第三步：处理子节点如果是字符串节点，那么直接设置textContent；如果是数组节点，那么遍历调用 mount 函数；

第四步：挂在到container上面 添加子节点 appendChild  将上面这个标签的内容 挂载到container
```

#### mount函数代码实现

```js
const h = (tag, props, children) => {
    //vnode就是一个JavaScript对象 h函数返回一个vnode
    return {
        tag,
        props,
        children
    }
}

const mount = (vnode, container) => {
    //vnode -element节点 
    //1.根据vnode创建一个元素 并且在vnode节点上也会保存一份真实dom
    const el = vnode.el = document.createElement(vnode.tag)
    //2.处理props 
    if (vnode.props) { //判断是否穿过来的有值 有可能传入null
        for (const key in vnode.props) {
            const value = vnode.props[key];

            if (key.startsWith("on")) { // 对事件监听的判断
                el.addEventListener(key.slice(2).toLowerCase(), value) //截掉头两位字母 on 并将驼峰式命名改成小写
            } else {
                el.setAttribute(key, value);  //设置属性
            }
        }
    }
    // 3.处理children
    if (vnode.children) {
        //判断类型 字符串直接设置
        if (typeof vnode.children === "string") {
            el.textContent = vnode.children;
        } else {
            //如果不是字符串的话 就是一个数组 数组里面不就是vnode节点么  在走一遍mount就完事了
            vnode.children.forEach(item => {
                //递归调用 将里面的内容挂载到el上面实现的tag节点
                mount(item, el);
            })
        }
    }
    // 4.将el挂载到container上  添加子节点 appendChild
    container.appendChild(el);
}
```

#### patch函数思路

patch函数，用于对两个VNode进行对比，决定如何处理新的VNode；

```js
1.先判断两者类型是不是一样的 如果两者标签类型都不一样的话 那么就移除掉原来的dom树，直接替换到新的dom树

2.如果两者标签一样 
2.1 取出element对象并且在n2中也进行保存 
2.2 处理props 
判断props是不是相同的 如果不相同的设置新节点的newValue ，删除旧节点的value 
2.3 处理children
判断newChildren是不是一个string类型的 如果是的话就代表是一串text文本 直接替换到el上面 
如果newChildren是一个数组 那么里面就是各个Vnode节点了 这样我们就要考虑oldChildren里面的东西了 
如果oldChildren里面是一个字符串类型的话，那么直接清空oldChildren 然后遍历一下newChildren数组里面的vnode节点 进行挂载就可以了
如果oldChildren里面是数组类型的话，那么就点和原来的进行比对（diff算法）最大可能性 复用原来的代码
```

#### patch函数实现

```js
const h = (tag, props, children) => {
  // vnode -> javascript对象 -> {}  vnode就是一个JavaScript对象
  return {
    tag,
    props,
    children
  }
}

//container 代表 div->#app
const mount = (vnode, container) => {
  // vnode -> element 将vnode转成element节点
  // 1.创建出真实的原生dom, 并且在vnode上保留el     document.createElement(vnode.tag);创建对应的元素
  const el = vnode.el = document.createElement(vnode.tag);

  // 2.处理props 
  if (vnode.props) {    //判断原因 props里面有可能是个空
    for (const key in vnode.props) {
      const value = vnode.props[key];

      if (key.startsWith("on")) { // 对事件监听的判断
        el.addEventListener(key.slice(2).toLowerCase(), value) //截掉头两位字母 on 并将驼峰式命名改成小写
      } else {
        el.setAttribute(key, value);  //设置属性
      }
    }
  }

  // 3.处理children
  if (vnode.children) {
    //判断类型 字符串直接设置
    if (typeof vnode.children === "string") {
      el.textContent = vnode.children;
    } else {
      //如果不是字符串的话 就是一个数组
      vnode.children.forEach(item => {
        //递归调用 将里面的内容挂载到子节点上
        mount(item, el);
      })
    }
  }

  // 4.将el挂载到container上  添加子节点 appendChild
  container.appendChild(el);
}



const patch = (n1, n2) => {
  //第一步判断类型是否一样  类型不同移除旧的节点 挂载新的节点
  if (n1.tag !== n2.tag) {
    //拿到当前元素的父元素 parentElement  才能移除掉当前的元素
    const n1ElParent = n1.el.parentElement;
    n1ElParent.removeChild(n1.el);  //移除子元素
    mount(n2, n1ElParent);  //将新节点重新挂载
  } else {
    // 1.取出element对象, 并且在n2中进行保存 因为el上面的值 要改变 比如class改变了
    const el = n2.el = n1.el;
    // 2.处理props  判断props是否有值 没有值赋值一个空的对象
    const oldProps = n1.props || {};
    const newProps = n2.props || {};
    // 2.1.获取所有的newProps添加到el
    for (const key in newProps) {
      const oldValue = oldProps[key];
      const newValue = newProps[key];
      //判断两个值是不是相同的 如果是相同的 就不需要重新设置属性
      if (newValue !== oldValue) {
        if (key.startsWith("on")) { // 对事件监听的判断
          el.addEventListener(key.slice(2).toLowerCase(), newValue)
        } else {
          el.setAttribute(key, newValue);
        }
      }
    }

    // 2.2.删除旧的props
    for (const key in oldProps) {
      if (key.startsWith("on")) { // 对事件监听的判断
        const value = oldProps[key];
        //移除时间需要value
        el.removeEventListener(key.slice(2).toLowerCase(), value)
      }
      if (!(key in newProps)) {
        el.removeAttribute(key);
      }
    }

    // 3.处理children  没有值的话给一个空的数组
    const oldChildren = n1.children || [];
    const newChidlren = n2.children || [];

    if (typeof newChidlren === "string") { // 情况一: newChildren本身是一个string 直接给之前的children来替换
      // 边界情况 (edge case)
      if (typeof oldChildren === "string") {
        if (newChidlren !== oldChildren) {
          el.textContent = newChidlren
        }
      } else {
        el.innerHTML = newChidlren;
      }
    } else { // 情况二: newChildren本身是一个数组

      if (typeof oldChildren === "string") {
        el.innerHTML = "";    //如果是一个string 给原来oldchildren清空
        newChidlren.forEach(item => {  //新数组里面的东西 进行挂载
          mount(item, el);
        })
        //oldChildren 和newChildren两者都是数组的情况下
      } else {
        // oldChildren: [v1, v2, v3, v8, v9]
        // newChildren: [v1, v5, v6] 
        /* 
        需要移动节点来进行patch对比
        */
        /* 
        * 拿到相对更短的长度遍历 如果是现在的vnode节点短的话 原来的vnode节点长 就进行删除操作 
        *  如果是现在的vnode节点长 就进行添加挂载操作
        */

        // 1.前面有相同节点的原生进行patch操作
        const commonLength = Math.min(oldChildren.length, newChidlren.length);
        for (let i = 0; i < commonLength; i++) {
          //调用patch 进行递归调用
          patch(oldChildren[i], newChidlren[i]);
        }

        // 2.newChildren.length > oldChildren.length
        if (newChidlren.length > oldChildren.length) {
          //对老的数组做一个切割遍历 然后挂载多出来的那两个节点
          newChidlren.slice(oldChildren.length).forEach(item => {
            mount(item, el);
          })
        }

        // 3.newChildren.length < oldChildren.length
        if (newChidlren.length < oldChildren.length) {
          //截出来新的vnode节点长度 进行卸载操作
          oldChildren.slice(newChidlren.length).forEach(item => {
            el.removeChild(item.el);
          })
        }
      }
    }
  }
}

```

### 响应式系统的实现

#### 什么是响应式？

``` 
当某一个值发生改变的时候 他所关联的函数自动重新执行 就是响应式 
比如vue3 
ref对象数值进行改变  然后render函数重新执行 
```

#### 1.封装一个响应式的函数

```js
为什么要封装一个响应式的函数呢？
因为在响应式函数里面 你不确定有些函数是响应式还是不是响应式的 ，如果我们封装一个响应式的函数  想让哪一个函数变成响应式的 
就将这个函数传入到我们封装的函数里面
eg：
function WatchFn(){
    //这里面能将代码变成响应式的 
}
//想要变成响应式的函数
const foo = function() {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
  }


WatchFn( foo) //传入foo函数 执行响应式
```

#### 2.封装一个装入响应式函数的数组

```js
为什么要整一个数组呢？
因为很多函数需要变成响应式的 可以将这些函数传入WatchFn的数组里面 这样就可以控制这些响应式函数了 
最后拿到这个数组 将所有响应式的函数进行一个执行

//封装一个响应式的数组
let reactiveFns = []  //name发生改变所需要执行的函数


//响应式函数里面的内容
function WatchFn(fn){
    reactiveFns.push(fn)
}


//对象的响应式 对象里面的值改变 下面跟他有关系的函数 就全部放到WatchFn里面执行
const obj= {
    name:'wyd',
    age:19
}

//传入匿名函数就行
WatchFn(function() {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
  })

watchFn(function() {
    console.log(obj.name, "demo function -------")
  })

function bar(){
console.log("普通的其他函数")
  console.log("这个函数不需要有任何响应式")
}

//遍历数组中的函数 所有函数执行一下
reactiveFns.forEach(fn=>{
    fn()
})
```

#### 3.依赖收集类的改变

```js
如果我们使用一个数组的话 他数组只能进行 name发生改变所需要执行的函数的收集 不完善 
如果age进行改变了呢 就不能使用这种单一数组的方式了 
所以我们可以选择一个 Class来进行这个操作 如果不同的属性给对应一个不同的实例对象  每一个实例对象里面都对应了 自己里面的数组。

//定义依赖的类
//Depend 依赖的意思 收集依赖
class Depend {
    constructor() {
        this.reactiveFns = [] //定义一个数组
    }
    //添加到响应式数组里面
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }

    //notify  通知 遍历执行一遍
    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }
    
}


//然后放入响应式函数的方法就点改变了 每一个属性对象一个depend对象
const depend = new Depend()

//响应式函数 放入这里面的 都是要变成响应式函数的方法
function watchFn(fn) {
    depend.addDepend(fn)
}

··················································································································
//里面的代码修改
// 对象的响应式 
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}

watchFn(function () {
    const newName = obj.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(obj.name) // 100行
})

watchFn(function () {
    console.log(obj.name, "demo function -------")
})


obj.name = 'wz'
··················································································································
//name改变 调用notify方法 
depend.notify()
```

#### 4.自动监听对象的变化

```js
现在对象改变了 我自己是手动进行depend.notify()执行函数的  
但是实际上应该是 如果我们的属性改变了 我是不是应该属性改变了的话 我自动去执行depend.notify() 那么就点监听对象的属性改变后 
在set里面 自动执行 notify了 就是数据改变了 然后在执行一遍函数

class Depend {
    constructor() {
        this.reactiveFns = []
    }
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }
    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }
}


const depend = new Depend()

//响应式函数 放入这里面的 都是要变成响应式函数的方法
function watchFn(fn) {
    depend.addDepend(fn)
}


//对象的响应式

const obj = {
    name: 'wyd',
    age: 19,
}


// 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function(target, key, receiver) {
      return Reflect.get(target, key, receiver)
    },
    set: function(target, key, newValue, receiver) {
      Reflect.set(target, key, newValue, receiver)
      depend.notify() //在这里面执行函数
    }
  })
  
..................................................................................................................
//执行函数
watchFn(function () {
    const newName = objProxy.name
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
})

watchFn(function () {
    console.log(objProxy.name, "demo function -------")
})

objProxy.name = "kobe"
objProxy.name = "james"
objProxy.name = "curry"
..................................................................................................................

```

#### 5.优化

```js
现在如果不光是obj.name改变的话 obj.age改变 但是所有函数都会跟着改变 不是指定的属性 对应指定的函数进行改变  
所以我们现在要优化一下
```

#### 6.依赖收集的管理

```js
现在考虑的就是 在开发过程中，应该一个属性对应一个depend（依赖），所以需要想到一种数据结构给这个数据存储上，并且在开发过程中，有可能不光只有一个对象是响应式的 ，比如vue3里面 可以通过ref/reactive创建多个响应式对象，按照这种思路，现在我们来优化下代码
使用weakMap和Map结构来存储，现在depend调用 全部都跟着调用，所以要修改一下获取最真实的depend，这个时候，封装一个可以获取真正depend的函数


class Depend {
    constructor() {
        this.reactiveFns = []
    }
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }
    notify() {
        this.reactiveFns.forEach(fn => {
            fn();
        })
    } 

}

// 封装一个响应式的函数
const depend = new Depend()
function watchFn(fn) {
   depend.addDepend(fn)
}


//封装一个真正获取depend函数（既可以收集依赖/获取依赖）WeakMap的原因是弱引用
const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据 target对象获取map
    let map = targetMap.get(target)
    //第一次调用没有map对象
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }
    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend //最后确定的depend对象
}

// 对象的响应式
const obj = {
    name: "why", // depend对象
    age: 18 // depend对象
}

//监听对象的属性变量 Proxy(vue3)/object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        return Reflect.get(target, key, receiver)
    },
    
    set: function (target, key, newValue, receiver) {
     Reflect.set(target, key, newValue, receiver)
     const depend =getDepend(target, key)
     depend.notify()
    }
})
...................................................................................................................
watchFn(function() {
    console.log("-----第一个name函数开始------")
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
    console.log("-----第一个name函数结束------")
  })
  
  watchFn(function() {
    console.log(objProxy.name, "demo function -------")
  })

objProxy.name = "kobe"
objProxy.name = "james"
objProxy.name = "curry"
objProxy.age = 100
...................................................................................................................
```

#### 7.正确的依赖收集

```js
现在depend收集依赖是什么样子的，是你只要传入响应式函数的话 就放到之前 const depend = new Depend对象里面
所有的依赖都放在一个depend里面了，但是本来我们应该name对应自己的depend，age对应自己的depend，放在一个里面这样看 反而是不对的
const depend = new Depend()
function watchFn(fn) {
   depend.addDepend(fn)
}
  将所有的依赖全部放在一个depend实例里面了 ，在这一步我们需要改变 ，应该根据watchFn里面使用到了谁，将放入一个自己的depend数组里面，但是我们如何知道到底用到了谁呢？这个时候就需要使用到代理对象里面的getter了，因为你想要获取值，肯定是要走proxy代理对象里面的捕获器的，所以可以在getter里面正确的收集依赖
。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。 
  class Depend {
    constructor() {
        this.reactiveFns = []
    }
    addDepend(reactiveFn) {
        this.reactiveFns.push(reactiveFn)
    }
    notify() {
        this.reactiveFns.forEach(fn => {
            fn();
        })
    } 

}

// 封装一个响应式的函数
let activeReactiveFn = null //封装一个全局变量，方便后面收集依赖
function watchFn(fn) {
  activeReactiveFn = fn
    fn()
  activeReactiveFn= null //制空 下一次收集方便
}


//封装一个真正获取depend函数（既可以收集依赖/获取依赖）
const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据 target对象获取map
    let map = targetMap.get(target)
    //第一次调用没有map对象
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }
    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend //最后确定的depend对象
}

// 对象的响应式
const obj = {
    name: "why", // depend对象
    age: 18 // depend对象
}

//监听对象的属性变量 Proxy(vue3)/object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        //从收集依赖的数据结构获取到本身的map对象 第一次没有的话 就直接创建了一个
        const depend = getDepend(target,key)
        //如何在这里获取到watchFn里面想要加入的函数 拿到全局的依赖加入
        depend.addDepend(activeReactiveFn)
        return Reflect.get(target, key, receiver)
    },
    
    set: function (target, key, newValue, receiver) {
     Reflect.set(target, key, newValue, receiver)
     const depend = getDepend(target, key)
     depend.notify()
    }
})
...................................................................................................................
watchFn(function() {
    console.log("-----第一个name函数开始------")
    console.log("你好啊, 李银河")
    console.log("Hello World")
    console.log(objProxy.name) // 100行
    console.log("-----第一个name函数结束------")
  })
  
  watchFn(function() {
    console.log(objProxy.name, "demo function -------")
  })

objProxy.name = "kobe"
objProxy.name = "james"
objProxy.name = "curry"

objProxy.age = 100
...................................................................................................................
```

#### 8.代码重构

```js
1.现在在Proxy里面还需要调用一次拿到全局对象 ，但是我们要是不想这么做的话，就需要在Depend class封装类里面进行封装一个函数 depend
2.还有就是 上面的我们是一个数组结构，但是数组里面可能会有重复的内容 ，所以选择set结构


//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/* 
Depend优化:
 1.depend方法 不在proxy/get 里面使用全局变量
 2.使用set来保存依赖函数,而不是数组[]
*/

class Depend {
    constructor() {
        /* 
    以前用数组封装 所以 只是添加进入数组 使用set 不会再次添加 
    使用set是因为保证 一个obj.name只执行一次
    set保证只执行一次
        */
        this.reactiveFns = new Set();
    }

    depend() {
        if (activeReactiveFn) {
            //set 方法使用add方法
            this.reactiveFns.add(activeReactiveFn);
        }
    }

    notify() {
        this.reactiveFns.forEach(fn => {
            fn()
        })
    }

}
//封装一个响应式的函数
function watchFn(fn) {
    activeReactiveFn = fn;
    fn()
    activeReactiveFn = null;
}

//封装一个获取depend函数

const targetMap = new WeakMap();
function getDepend(target, key) {
    //根据target对象获取map的过程
    let map = targetMap.get(target)
    if (!map) {
        map = new Map()
        targetMap.set(target, map)
    }

    //根据key获取depend对象
    let depend = map.get(key)
    if (!depend) {
        depend = new Depend()
        map.set(key, depend)
    }
    return depend
}


// 对象的响应式
const obj = {
    name: "wyd", // depend对象
    age: 18 // depend对象
}


//监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = new Proxy(obj, {
    get: function (target, key, receiver) {
        //根据target.key获取对应的depend
        const depend = getDepend(target, key);
        // 给depend对象中添加响应函数
        // 优化代码 不需要对代码进行添加全局变量 只需要调用一个depend方法就行 以前的添加数组的方法就用不到了
        // 删除掉addDepend方法
        // depend.addDepend(activeReactiveFn) 
       
        depend.depend()

        return Reflect.get(target,key,receiver)
    },
    set:function(target,key,newValue,receiver){
        Reflect.set(target,key,newValue,receiver)
        const depend= getDepend(target,key)
        depend.notify()
    }
})

watchFn(()=>{
    console.log(objProxy.name, "-------")
  console.log(objProxy.name, "+++++++")
}) 

objProxy.name = "kobe"
```

#### 9.保证相应式自动化的函数 

```js
因为我们现在只是创建一个Proxy的对象才是响应式 如果想要vue3的reactive那种的话 创建响应式函数的话 就需要在外面包上一个函数 
const objProxy = new Proxy(obj, {})

//保存当前需要收集的响应式函数
let activeReactiveFn = null;

/**
 * Depend优化:
 *  1> depend方法
 *  2> 使用Set来保存依赖函数, 而不是数组[]
 */

 class Depend {
    constructor() {
      this.reactiveFns = new Set()
    }
  
    // addDepend(reactiveFn) {
    //   this.reactiveFns.add(reactiveFn)
    // }
  
    depend() {
      if (activeReactiveFn) {
        this.reactiveFns.add(activeReactiveFn)
      }
    }
  
    notify() {
      this.reactiveFns.forEach(fn => {
        fn()
      })
    }
  }
  

  //封装一个响应的函数

  function watchFn(fn){
      activeReactiveFn = fn;
      fn();
      activeReactiveFn= null;
  }

  //封装一个获取depend的函数
  const targetMap = new WeakMap();
 function getDepend(target,key){
     //根据target对象获取map的过程
     let map = targetMap.get(target)
     if (!map) {
         map = new Map()
         targetMap.set(target,map);
     }

     //根据key获取depend对象
     let depend = map.get(key)
     if (!depend) {
          depend= new Depend();
          map.set(key,depend);
     }
     return depend;
 }

 //保证自动化响应式的函数

 function reactive(obj){
     return new Proxy(obj,{
       get:function(target,key,receiver){
           //根据target，key获取对象的depend
           const depend =getDepend(target,key)
           //给depend 对象中添加响应函数
           // depend.addDepend(activeReactiveFn)
           depend.depend();
           return Reflect.get(target,key,receiver)
       },
       set:function(target,key,newValue,receiver){
           Reflect.set(target,key,newValue,receiver);
           const depend =getDepend(target,key);
           depend.notify()
       }
     })
 }

 // 监听对象的属性变量: Proxy(vue3)/Object.defineProperty(vue2)
const objProxy = reactive({
    name: "why", // depend对象
    age: 18 // depend对象
  })
  
  const infoProxy = reactive({
    address: "广州市",
    height: 1.88
  })

  watchFn(()=>{
      console.log(infoProxy.address);
  })

  infoProxy.address = "北京市"

  const foo = reactive({
    name: "foo"
  })
  
  watchFn(() => {
    console.log(foo.name)
  })
  
  foo.name = "bar"
   
```

