//接收一个根组件
function createApp(rootComponent) {
    return {
        //返回挂载对象
        mount(selector) {
            const container = document.querySelector(selector);
            //第一次进入是挂载根组件 第二次进入是对根组件进行刷新
            let isMounted = false;
            let oldVNode = null; //保存一下旧的vnode

            //内部数据发生改变的话  就需要使用我们这个函数了 因为是响应式的🐎
            watchEffect(function () {
                //没挂载的基础上 进行挂载
                if (!isMounted) {
                    oldVNode = rootComponent.render(); //调用APP组件中的 render函数 返回一个vnode节点
                    mount(oldVNode, container);
                    isMounted = true; //设置为true 挂载过了
                } else {
                    const newVNode = rootComponent.render(); //拿到最新的vnode 调一下render函数
                    patch(oldVNode, newVNode); //patch
                    oldVNode = newVNode; //为了在下一次进行patch 再次存储一下oldVnode节点
                }
            })
        }
    }
}