import  { markResultType } from "../../../utils/constants"

export const props = {
      //组件v-model绑定值
    value: {
      type: String,
      default: '',
    },
    showLength: {
      type: Boolean,
      default: false,
    },
    //高度自适应
    autoHeight: {
      type: Boolean,
      default: false,
    },
    //是否自动获取焦点
    autofocus: {
      type: Boolean,
      default: false,
    },
    placeholder: String,
    readonly: {
      type: Boolean,
      default: false,
    },
    //可见高度
    rows: {
      type: Number,
      default: 0,
    },
    maxlength: Number,
    disabled: {
      type: Boolean,
      default: false,
    },
    //可见宽度
    cols: {
      type: Number,
      default: 0,
    },
    /** 禁止textarea拉伸 */
    // resize: none;
    resize: {
      type: String,
      default: 'both',
    },
    state: {
      type: Number,
      default: markResultType.DEFAULT,
  },
}